export function setIdToLocalStorage(item: string): void {
    const items = localStorage.getItem("movieLikeIds");
    if(items) {
        let movieLikeIds: string[] = JSON.parse(items)
        if(isItemInLocalStorage(item)) {
            movieLikeIds = JSON.parse(items).filter((i: string) => !(i === item))
        } else {
            movieLikeIds.push(item);
        }
        localStorage.removeItem("movieLikeIds");
        localStorage.setItem("movieLikeIds",  JSON.stringify(movieLikeIds));
    } else {
        localStorage.setItem("movieLikeIds", JSON.stringify([item]));
    }
}

export function isItemInLocalStorage(item: string): boolean {
    const items = localStorage.getItem("movieLikeIds");
    if(items) {
        return JSON.parse(items).includes(item)
    }
    return false
}

export function getMoviesLikesIds(): string[] {
    const items = localStorage.getItem("movieLikeIds");
    return items ? JSON.parse(items) : [];
}