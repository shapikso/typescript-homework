export function getURLOfMovie(typeOfMovies: 'top_rated' | 'popular' | 'upcoming'| string, page: number): string {
    return `https://api.themoviedb.org/3/movie/${typeOfMovies}?api_key=540222d6e93aeb84024896a2fb996769&page=${page}` //https://api.themoviedb.org/3/movie/{movie_id}?api_key=
}

export function searchMovie(typeOfMovies: string, page:number): string {
    return `https://api.themoviedb.org/3/search/movie?api_key=540222d6e93aeb84024896a2fb996769&query=${typeOfMovies}&page=${page}`
}
