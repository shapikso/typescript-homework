export interface Movie {
    poster_path: string, 
    overview: string,
    id: number,
    release_date: string,
    title: string,
    backdrop_path: string,
}