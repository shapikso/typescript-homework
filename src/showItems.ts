
import { Movie } from './interface';
import { isItemInLocalStorage, setIdToLocalStorage } from './helpers/localStorageHelper';

export async function showItems(
    data: Movie[], pageNumber: number, movieIsInFavorite: boolean) {
        const films = document.getElementById('film-container');
        const filmsFavorite = document.getElementById('favorite-movies');
           
        
        if(movieIsInFavorite){
            if(!!filmsFavorite) {
                if(pageNumber < 2){
                filmsFavorite.innerHTML = ' ';}
                console.log(data);
                const filmById: any = data;
            createItem(filmsFavorite, filmById, true);
        }
        }
        else {
        if(!!films) {
            if(pageNumber < 2){
                films.innerHTML = ' ';
            }
        data.forEach((movie: Movie) => createItem(films, movie, false));
      }
    }
  }


  function createItem (films: HTMLElement, movie: Movie, favorite: boolean) {
    const {poster_path, overview, release_date, id, title, backdrop_path} = movie;
    const movieLikeId = `movie-like-${id}`;
    const movieEl = document.createElement('div')
    if(!favorite){
        movieEl.classList.add('col-lg-3', 'col-md-4');
    }
    if(getRandomInt(5) === 2) {
        showRandomPreview(title, backdrop_path, overview);
    }
    movieEl.classList.add('col-12', 'p-2'); // col-12 p-2
    movieEl.innerHTML = `
                    <div class="card shadow-sm">
                        <img
                            src="https://image.tmdb.org/t/p/original/${poster_path}"
                        />
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        cursor="pointer"
                        stroke="red"
                        fill= ${isItemInLocalStorage(id.toString()) ? "red" : "#ff000078"}
                        width="50"
                        height="50"
                        class="bi bi-heart-fill position-absolute p-2"
                        viewBox="0 -2 18 22"
                        id="${movieLikeId}"
                    >
                        <path
                            fill-rule="evenodd"
                            d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                        />
                    </svg>
                        <div class="card-body">
                            <p class="card-text truncate">
                               ${overview}
                            </p>
                            <div
                                class="
                                    d-flex
                                    justify-content-between
                                    align-items-center
                                "
                            >
                                <small class="text-muted"> ${release_date}</small>
                            </div>
                        </div>
                    </div>
    `
    films.appendChild(movieEl);
    const movieLikeIdEl = document.getElementById(movieLikeId);
    if(movieLikeIdEl) {
        movieLikeIdEl.addEventListener('click', (event) => {
            setIdToLocalStorage(id.toString());
            movieLikeIdEl.style.fill = isItemInLocalStorage(id.toString()) ? "red" : "#ff000078";
        });
    }
}

 function showRandomPreview(title: string, backdrop_path: string, overview: string): void {
     const randomMovie = document.getElementById('random-movie');
     const randomMovieName = document.getElementById('random-movie-name');
     const randomMovieDescription = document.getElementById('random-movie-description');

     if(randomMovie && randomMovieName && randomMovieDescription)
     {  
        randomMovie.style.backgroundImage = `url(https://image.tmdb.org/t/p/original//${backdrop_path})`;
       // randomMovie.style.backgroundRepeat = 'no-repeat';
        randomMovie.style.backgroundSize = '100% 900px';
        randomMovieName.innerText = `${title}`;
        randomMovieDescription.innerText = `${overview}`;
     }

 }


 function getRandomInt(max: number): number {
    return Math.floor(Math.random() * max);
  }