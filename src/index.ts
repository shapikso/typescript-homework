import {Movie} from './interface';
import {showItems} from './showItems'
import {getURLOfMovie, searchMovie} from './request'
import { getMoviesLikesIds } from './helpers/localStorageHelper';

export async function render(): Promise<void> {
    // TODO render your app here
    const upcoming = document.getElementById('upcoming');
    const popular = document.getElementById('popular');
    const topRated = document.getElementById('top_rated');
    const submit = document.getElementById('submit');
    const loadMore = document.getElementById('load-more'); 
    const favorite = document.querySelector('.navbar-toggler');
    let pageNumber: number = 2;
    let showingMovies: string ='popular';
    let numberOfFavorite: number = 1;
   
    click(showingMovies, 1);

    if(upcoming) {
        upcoming.addEventListener('click', (event) =>{
            showingMovies ='upcoming';
            pageNumber = 2;
            click(showingMovies,1);
        });
    }

    if(popular) {
        popular.addEventListener('click', (event) =>{
            showingMovies ='popular';
            pageNumber = 2;
            click(showingMovies,1);
        });
    }
    if(topRated) {
        topRated.addEventListener('click', (event) =>{
            showingMovies ='top_rated';
            pageNumber = 2;
            click(showingMovies,1);
        });
    }

    if(submit) {     
        submit.addEventListener('click', (event) =>{
            
            pageNumber = 2;
            let searchText  = (<HTMLInputElement>document.getElementById('search')).value;
           console.log(searchText);
            getMovies(searchMovie((searchText as string),1),1).then((data: Movie[]) => {
                showItems(data,1,false);
                
            });
        });

     }

    if(loadMore){
        loadMore.addEventListener('click', (event) =>{
            click(showingMovies,pageNumber);
            pageNumber++;
        });
       
    }

    if(favorite) {
        favorite.addEventListener('click', (event) =>{
            let data: object[] =[];
            getMoviesLikesIds().forEach((i:string)=>
            {   
              getMovies(getURLOfMovie(i, 1),2).then((data: Movie[]) => {  
                showItems(data, numberOfFavorite,true); 
                numberOfFavorite++;
                });

            });
          numberOfFavorite = 1;
        });
       
    }
}

function click(showingMovies: string, pageNumber: number): void {
    getMovies(getURLOfMovie(showingMovies, pageNumber),1).then((data: Movie[]) => {
        showItems(data, pageNumber,false); 
    });
}

async function getMovies(
    URL: string,
    typeOfreturning: 1 | 2
  ): Promise<Movie[]> {
    const response = await fetch(URL);
    const body = await response.json();
    if(typeOfreturning === 1){ 
        return body.results;}
    else {
        return body;
    }
  }

  